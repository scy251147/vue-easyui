
var jsonData = function(status,message,response){
                this.status = status;
                this.message = message;
                this.response = response;
            };

jsonData.prototype.status = null;
jsonData.prototype.message = null;
jsonData.prototype.response = null;

module.exports = jsonData