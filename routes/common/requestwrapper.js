
var request = require('request');
var json = require('./jsonwrapper.js');

module.exports = {
  // perform GET request
  get: function(url) {
    request.get(url, function (error, response, body) {
        var returnJson = new json(null,null,null);
        if (error) {
            console.error('request [%s] failed: ', url, error);
        } else {
            var responseJson = JSON.parse(body);
            returnJson.status = responseJson.status;
            returnJson.message = responseJson.message;
            returnJson.response = responseJson.response;
            return returnJson;
        }
    });
  },

  post:function(url,data){
      request.post(url,data,function(error,response,body){
        var returnJson = new json(null,null,null);
        if (error) {
            console.error('request [%s] failed: ', url, error);
        } else {
            var responseJson = JSON.parse(body);

            returnJson.status = responseJson.status;
            returnJson.message = responseJson.message;
            returnJson.response = responseJson.response;
        }
        return returnJson;
      });
  }
};