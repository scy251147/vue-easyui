//express component
var express = require('express');
//router component
var router = express.Router();
//request component to send request
var request = require('request');
//return json format
var json = require('./common/jsonwrapper');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('5-dynamic-datagrid-requirejs', { title: 'easyui datagrid managed by require.js' });
});

//Get the templates by pager and searcher
router.get('/addresses', function (req, res, next) {
    //reconstruct the url for pagger
    var addresses_url = "http://10.154.81.116:8139/address/userAddress/getUserAddress.json";
    var page_number = req.query.page;
    var rows_count = req.query.rows;
    console.log(page_number+"|"+rows_count);
    var options = {
                    uri: addresses_url,
                    json: {
                            userId:req.query.userid,
                          }
                  };
    //perform the action to get data from server side
    request.post(options, function (error, response, body) {
        //error handle
        if(error){
            console.error('request [%s] failed: ', templates_url, error.stack);
            return next(error);
        }
        //if problem with data from server side, throw it directly to front
        if(body.status !== "0") {
            res.json(new json(body.status,body.message,body.response));
        }
        //if no problem with data from server side 
        else {
             var returnJson = {
                    total:null,
                     rows:null
                };
            //construct the data that Jquery EasyUI needs
            var len = body.response.length;
            var rows = body.response;

            var start_count = (page_number-1)*rows_count;
            var end_count = page_number*rows_count-1;

            var array = [];

            for(var i=start_count;i<=end_count;i++)
            {
                if(rows[i]!==undefined) {
                    array.push(rows[i]);
                }
            }

            returnJson.rows = array;
            returnJson.total = len;
            //post to front
            res.json(returnJson);
        }
    });
});

module.exports = router;
