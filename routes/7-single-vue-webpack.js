var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('7-single-vue-webpack', { title: 'single vue file resolved by webpack' });
});

module.exports = router;
