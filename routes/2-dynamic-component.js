var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('2-dynamic-component', { title: 'dynamic vue component' });
});

module.exports = router;
