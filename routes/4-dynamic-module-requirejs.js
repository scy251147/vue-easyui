var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('4-dynamic-module-requirejs', { title: 'dynamic modules managed by require.js' });
});

module.exports = router;
