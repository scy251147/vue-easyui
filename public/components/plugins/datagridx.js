(function ($) {
    $.fn.datagridx = function(options) {

        var settings = $.extend({
                                    url            : FLAG_EMPTY,            //remote url
                                    pageSize       : 10,                    //reocord size per page
                                    pageList       : [10, 20, 30],          //record size list
                                    method         : FLAG_GET,              //method to get data
                                    columns        : FLAG_EMPTY,            //columns
                                    buttons        : [FLAG_ADD_BUTTON,FLAG_EDIT_BUTTON,FLAG_ENABLE_BUTTON,FLAG_REFRESH_BUTTON],
                                    doDoubleClick  : function(){},          //double click row action
                                    doAdd          : function(){},          //add record action
                                    doEdit         : function(){},          //edit record action
                                    doRemove       : function(){},          //remove record action
                                    elementName    : this.selector          //binding element name
                                }, options );
        
        var plugin = this;

        var addButton     = {text: 'Add',iconCls: 'icon-add',handler: function(){plugin.addHandler();}};
        var editButton    = {text: 'Edit',iconCls: 'icon-edit',handler: function () {plugin.editHandler();}};
        var enableButton  = {text: 'Enable/Disable',iconCls: 'icon-enable',handler: function () {plugin.removeHandler();}};
        var refreshButton = {text: 'Refresh',iconCls: 'icon-reload',handler: function () {plugin.refreshHandler(); }};
        var delimiter     = '-';

        //init method befor render the plugin
        plugin.init = function(){
            //attach dynamic button collection
            settings.buttonCollection=[];
            //loop user input buttons
            $.each(settings.buttons,function(index,button){
                if(button===FLAG_ADD_BUTTON) {
                    settings.buttonCollection.push(addButton);
                    settings.buttonCollection.push(delimiter);
                }
                if(button===FLAG_EDIT_BUTTON){
                    settings.buttonCollection.push(editButton);
                    settings.buttonCollection.push(delimiter);
                }
                if(button===FLAG_ENABLE_BUTTON){
                    settings.buttonCollection.push(enableButton);
                    settings.buttonCollection.push(delimiter);
                }
                if(button===FLAG_REFRESH_BUTTON){
                    settings.buttonCollection.push(refreshButton);
                    settings.buttonCollection.push(delimiter);
                }
            });
        }

        //dynamic init toobar
        plugin.init();

        $(this).datagrid({
            url          : settings.url,
            height       : 'auto',
            nowrap       : false,
            striped      : true,
            border       : true,
            collapsible  : true,
            fit          : true,
            fitColumns   : true,
            multiSort    : true,
            remoteSort   : true,              //false：local sort，no need to code；true，remote sort，need code，will send ?page=1&rows=5&sort=firstName&order=asc request
            singleSelect : true,              //select single row
            pagination   : true,              //start page
            pageSize     : settings.pageSize, //size per page
            pageList     : settings.pageList, //page list 
            method       : settings.method,   //ajax method
            rownumbers   : true,              //line number
            onDblClickRow: function (rowIndex, rowData) {plugin.doubleClickHandler(rowIndex,rowData);},
            toolbar      : settings.buttonCollection,
            columns      : settings.columns
        });

        //Click the record
        plugin.doubleClickHandler = function(rowIndex,rowData){
            settings.doDoubleClick(rowIndex, rowData); 
        }

        //Add the record
        plugin.addHandler = function(){
            settings.doAdd();
        }

        //Edit the record
        plugin.editHandler = function(){
            var row = $(settings.elementName).datagrid('getSelected');
            if (row) {
                settings.doEdit(row);
            }
            else{
                $.messager.alert('Warnning', 'Please select one record to perform edit action!',"warning");
            }
        }

        //Remove the record
        plugin.removeHandler = function(){
            var row = $(settings.elementName).datagrid('getSelected');
            if(row) {
                settings.doRemove(row);
            }
            else{
                $.messager.alert('Warnning', 'Please select one record to perform the action!',"warning");
            }
        }

        //Refresh the records
        plugin.refreshHandler = function(){
            $(settings.elementName).datagrid('reload');
        }
    };
}(jQuery));