define(["Vue"],function(Vue){
    
    var datagridExtended = Vue.extend({
        props:{
            gridid: {
                    type: String,
                    required: true
                }
        },
        template:'<div :id="gridid" ></div>',
        data: function(){
            return{
                dataurl : "",
                datacol : [],
            }
        },
        //method one,use mounted to directly do bind
        mounted:function(){
            var dgId = "#" + this.gridid;
            bind(dgId, this.dataurl, this.datacol);
        },
        //watch changed data and handle
        watch:{
            //when grid data changes
            dataurl:function(newurl){
                var elementid = "#" + this.gridid;
                bind(elementid, newurl, this.datacol);
            },
            //when column data changes
            datacol:function(newcol){
                var elementid = "#" + this.gridid;
                bind(elementid, this.dataurl, newcol);
            }
        }
    });

    //bindings
    var bind = function(elementId,url,column){
         $(elementId).datagrid({
                                    url          : url,
                                    method       : "GET",
                                    border       : false,
                                    fit          : true,
                                    fitColumns   : true,
                                    singleSelect : true,                //select single row
                                    pagination   : true,                //start page
                                    pageSize     : 10,                  //size per page
                                    pageList     : [10, 20, 30],        //page list 
                                    rownumbers   : true,                //line number
                                    columns      : column
                                });
    }
    //component define ok
    Vue.component("vue-datagrid",datagridExtended);
    //new instance
    var mygrid = new Vue({
        el: '#dgContainer'
    });
    //return the object
    return mygrid;
});