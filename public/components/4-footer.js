define(["Vue"],function(Vue){

    var footer = Vue.extend({
        template:'<div class="footer">\
                        <div class="footer-main">\
                            <p>taoqun个人博客 | 记录 | 展示 | 使用vue\
                                <a href="mailto:taoquns@foxmail.com">联系我：{{name}}</a>\
                            </p>\
                        </div>\
                  </div>',
        data:function(){
            return{
                name:"this is the footer"
            }
        },
        methods:{
            show:function(){
                alert(this.name);
            }
        }
    });

    Vue.component("dy-require-footer",footer);

    var myfooter = new Vue({
        el:"#rfooter"
    });

    return myfooter.$children[0];
});