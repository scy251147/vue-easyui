var footer = Vue.extend({
    template:'<div class="footer">test footer test footer</div>',
    data:function(){
        return{
            name:"this is the footer"
        }
    },
    methods:{
        show:function(){
            alert(this.name);
        }
    }
});

Vue.component("my-footer",footer);

new Vue({
    el:"#footer"
});