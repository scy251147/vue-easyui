/**
 * Created by shichaoyang on 2017/5/2.
 */
require.config({
    //required css mapping, refer: http://www.bbsmax.com/A/QW5YxBo3Jm/
    map:{
        '*': {
            'css': '../../require-css/css'
        }
    },
    shim:{
        "easyui":{
            deps:[
                "jquery",
                "css!../../jquery-easyui/themes/default/easyui.css",
                "css!../../jquery-easyui/themes/icon.css"
            ]
        },
        "gridx":{
            deps:[
                "Vue",
                "easyui",
                "css!../stylesheets/gridstyle.css"
            ]
        }
    },
    paths:{
        "Vue"    :  "http://cdn.bootcss.com/vue/2.2.6/vue",
        "jquery" :  "../../jquery-easyui/jquery.min",
        "easyui" :  "../../jquery-easyui/jquery.easyui.min",
        "gridx"  :  "../../components/5-vue-datagrid"
    }
});