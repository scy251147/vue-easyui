require(['5-base'], function () {
    require(['data_grid']);
});

define('data_grid',["jquery","gridx"],function($,gridx){
    //render the easyui elements
    $.parser.parse();

    var columnData = [[
        { field: 'ADDRESS_ID'    , title: 'ID'         , width: 75                },
        { field: 'ADDRESS_NAME'  , title: 'name'       , width: 100, sortable:true},
        { field: 'RECEIVER'      , title: 'receiver'   , width: 75 , sortable:true},
        { field: 'MOBILE'        , title: 'mobile'     , width: 45 , sortable:true},
        { field: 'ADDRESS_DETAIL', title: 'address'    , width: 45 , sortable:true}
    ]];

    var columnDatax = [[
        { field: 'ADDRESS_ID'    , title: 'ID'         , width: 75                },
        { field: 'ADDRESS_NAME'  , title: 'name'       , width: 100, sortable:true},
        { field: 'ADDRESS_DETAIL', title: 'address'    , width: 45 , sortable:true}
    ]];

    var url = "dynamicdatagridrequirejs/addresses?userid=12345678901";

    gridx.$children[0].dataurl = url;
    gridx.$children[0].datacol = columnData;

    gridx.$children[1].dataurl = url;
    gridx.$children[1].datacol = columnData;

    setTimeout(function(){
        gridx.$children[0].datacol = columnDatax;
    },2000);
});