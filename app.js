var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var basicusage                = require('./routes/1-basic-usage');
var dynamiccomponent          = require('./routes/2-dynamic-component');
var dynamicmodule             = require('./routes/3-dynamic-module');
var dynamicmodulerequirejs    = require('./routes/4-dynamic-module-requirejs');
var dynamicdatagridrequirejs  = require('./routes/5-dynamic-datagrid-requirejs');
var simplewebpack             = require('./routes/6-simple-webpack');
var singlevuewebpack          = require('./routes/7-single-vue-webpack');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// don't format the html
console.log(app.get('env'));
if(app.get('env')==='development'){
    app.locals.pretty = true;
}

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'bower_components')));
app.use(express.static(path.join(__dirname, 'node_modules')));

app.use('/basicusage'              , basicusage);
app.use('/dynamiccomponent'        , dynamiccomponent);
app.use('/dynamicmodule'           , dynamicmodule);
app.use('/dynamicmodulerequirejs'  , dynamicmodulerequirejs);
app.use('/dynamicdatagridrequirejs', dynamicdatagridrequirejs);
app.use('/simplewebpack'           , simplewebpack);
app.use('/singlevuewebpack'        , singlevuewebpack);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
